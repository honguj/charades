package vn.tictocproduction.charades.extra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.debug.Debug;

import android.graphics.Color;
import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.Match;
import vn.tictocproduction.charades.base.TeamPlayer;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.ResourcesManager;
import vn.tictocproduction.charades.manager.SceneManager;
import vn.tictocproduction.charades.scene.MainMenuScene;

public class DualModeWindow extends Sprite{

	private static final float TITLE_X = GameConstants.SCREEN_CENTER_X - 80;
	private static final float TITLE_Y = GameConstants.SCREEN_HEIGHT - 130;
	private static final float PADDING_LEFT = 200;
	private static final float SPACE_BETWEEN_COLUMN = 10;

	private int selectedRound;
	private int selectedTeam;
	private List<Rectangle> teamOptions;
	private List<Rectangle> roundOptions;

	// UI Elements
	private Font titleFont;
	private Font textFont;
	private ButtonSprite playButton;
	private ButtonSprite exitButton;
	private MainMenuScene scene;

	public DualModeWindow(BaseScene s){
		super(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y,
				GameConstants.POPUP_WINDOW_WIDTH, GameConstants.POPUP_WINDOW_HEIGHT,
				ResourcesManager.getInstance().dual_mode_region,
				ResourcesManager.getInstance().vbom);
		selectedRound = GameConstants.DUAL_ROUND_CHOICES[0];
		selectedTeam = GameConstants.DUAL_TEAM_CHOICES[0];
		teamOptions = new ArrayList<Rectangle>();
		roundOptions = new ArrayList<Rectangle>();
		scene = (MainMenuScene)s;
		createButtons();
		createHeader();
		createRoundOptions();
		createTeamsOptions();
	}

	public int getRounds() { return selectedRound; }
	public int getTeams() { return selectedTeam; }

	private void createHeader(){
		FontFactory.setAssetBasePath("font/");
		final ITexture titleTexture = new BitmapTextureAtlas(ResourcesManager.getInstance().activity.getTextureManager(), 
				512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		titleFont = FontFactory.createStrokeFromAsset(ResourcesManager.getInstance().activity.getFontManager(), 
				titleTexture, ResourcesManager.getInstance().activity.getAssets(), "arial.ttf", 50, true, 
				Color.WHITE, 2, Color.parseColor("#c9cb1d"));
		titleFont.load();

		final ITexture intructionTexture = new BitmapTextureAtlas(ResourcesManager.getInstance().activity.getTextureManager(), 
				512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		textFont = FontFactory.createStrokeFromAsset(ResourcesManager.getInstance().activity.getFontManager(), 
				intructionTexture, ResourcesManager.getInstance().activity.getAssets(), "arial.ttf", 30, true, 
				Color.RED, 2, Color.parseColor("#c9cb1d"));
		textFont.load();

		Text header = new Text(TITLE_X, TITLE_Y, titleFont,
				"Dual Mode", new TextOptions(HorizontalAlign.CENTER), 
				ResourcesManager.getInstance().vbom);
		attachChild(header);
	}

	private void createTeamsOptions(){
		Text teamSelection = new Text(PADDING_LEFT, TITLE_Y - 40, textFont,
				"Teams:", new TextOptions(HorizontalAlign.CENTER), 
				ResourcesManager.getInstance().vbom);
		attachChild(teamSelection);

		int boxX = (int)PADDING_LEFT;
		int boxY = (int)teamSelection.getY() - 60;

		for (int i = 0; i < GameConstants.DUAL_TEAM_CHOICES.length; i++){
			final int teamsSelection = GameConstants.DUAL_TEAM_CHOICES[i];
			// Create touchable box
			Rectangle box = new Rectangle(boxX, boxY, GameConstants.DUAL_OPTION_SIZE, 
					GameConstants.DUAL_OPTION_SIZE, 
					ResourcesManager.getInstance().vbom){
				@Override
				public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
					selectedTeam = teamsSelection;
					updateColor(teamOptions, this);
					return false;
				}
			};
			if (teamsSelection == GameConstants.DUAL_TEAM_CHOICES[0])
				box.setColor(GameConstants.DUAL_SELECTED_COLOR);
			else
				box.setColor(GameConstants.DUAL_UNSELECTED_COLOR);
			attachChild(box);

			// Add text to box
			Text word = new Text(boxX, boxY, textFont, 
					teamsSelection+"", ResourcesManager.getInstance().vbom);
			attachChild(word);

			// Insert to options map
			teamOptions.add(box);

			scene.registerTouchArea(box);
			boxX += GameConstants.DUAL_OPTION_SIZE + SPACE_BETWEEN_COLUMN;
		}
	}

	private void updateColor(List<Rectangle> elements, Rectangle selected){
		for (int i = 0; i < elements.size(); i++){
			if (elements.get(i).equals(selected)){
				elements.get(i).setColor(GameConstants.DUAL_SELECTED_COLOR);
				Debug.e("DualModeUpdateColor","SELECTED");
			}
			else{
				elements.get(i).setColor(GameConstants.DUAL_UNSELECTED_COLOR);
				Debug.e("DualModeUpdateColor","UNSELECTED");
			}
		}
	}

	private void createRoundOptions(){
		Text roundSelection = new Text(PADDING_LEFT, TITLE_Y - 150, textFont,
				"Rounds:", new TextOptions(HorizontalAlign.CENTER), 
				ResourcesManager.getInstance().vbom);
		attachChild(roundSelection);

		int boxX = (int)PADDING_LEFT;
		int boxY = (int)roundSelection.getY() - 60;

		for(int i=0; i < GameConstants.DUAL_ROUND_CHOICES.length; i++){
			final int roundOption = GameConstants.DUAL_ROUND_CHOICES[i];
			Rectangle box = new Rectangle(boxX, boxY, GameConstants.DUAL_OPTION_SIZE, 
					GameConstants.DUAL_OPTION_SIZE, 
					ResourcesManager.getInstance().vbom){
				@Override
				public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
					selectedRound = roundOption;
					updateColor(roundOptions, this);
					return false;
				}
			};
			
			if (roundOption == GameConstants.DUAL_ROUND_CHOICES[0])
				box.setColor(GameConstants.DUAL_SELECTED_COLOR);
			else
				box.setColor(GameConstants.DUAL_UNSELECTED_COLOR);
			attachChild(box);

			// Add text to box
			Text word = new Text(boxX, boxY, textFont, 
					roundOption+"", ResourcesManager.getInstance().vbom);
			attachChild(word);

			// Insert to options map
			roundOptions.add(box);

			scene.registerTouchArea(box);
			boxX += GameConstants.DUAL_OPTION_SIZE + SPACE_BETWEEN_COLUMN;
		}
	}

	private void createButtons() {
		// Play button
		playButton = new ButtonSprite(GameConstants.SCREEN_CENTER_X - 90, 
				GameConstants.SCREEN_CENTER_Y - 190,
				ResourcesManager.getInstance().dual_play_button_region,
				ResourcesManager.getInstance().vbom
				);
		playButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
					float pTouchAreaLocalY) {
				// Setup for dual mode
				ResourcesManager.getInstance().match = new Match(selectedRound);
				
				for (int i = 0; i < selectedTeam; i++){
					TeamPlayer team = new TeamPlayer("Team " + i);
					ResourcesManager.getInstance().match.addTeam(team);
				}
				
				// Display Category Scene
				disposeWindows();
				SceneManager.getInstance().loadCategoryScene(ResourcesManager.getInstance().engine);
			}
		});
		playButton.setVisible(true);
		attachChild(playButton);
		SceneManager.getInstance().getCurrentScene().registerTouchArea(playButton);

		// Exit button
		exitButton = new ButtonSprite(GameConstants.POPUP_EXIT_BUTTON_X,
				GameConstants.POPUP_EXIT_BUTTON_Y,
				ResourcesManager.getInstance().dual_close_button_region,
				ResourcesManager.getInstance().vbom
				);
		exitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
					float pTouchAreaLocalY) {
				disposeWindows();
				// Reactivate menu scene
				scene.createMenuItems();
				scene.getMenuScene().setOnMenuItemClickListener(scene);
			}
		});
		exitButton.setVisible(true);
		attachChild(exitButton);
		SceneManager.getInstance().getCurrentScene().registerTouchArea(exitButton);
	}

	private void disposeWindows(){
		titleFont.unload();
		textFont.unload();
		SceneManager.getInstance().getCurrentScene().unregisterTouchArea(playButton);
		SceneManager.getInstance().getCurrentScene().unregisterTouchArea(exitButton);
		SceneManager.getInstance().getCurrentScene().detachChild(this);
	}
}
