package vn.tictocproduction.charades.extra;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.debug.Debug;

import android.graphics.Color;
import vn.tictocproduction.charades.base.Category;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.ResourcesManager;
import vn.tictocproduction.charades.manager.SceneManager;
import vn.tictocproduction.charades.scene.CategoryScene;

public class InstructionWindow extends Sprite{

	private static final float TITLE_X = GameConstants.SCREEN_CENTER_X - 80;
	private static final float TITLE_Y = GameConstants.SCREEN_HEIGHT - 130;
	private static final float INSTRUCTION_X = GameConstants.SCREEN_CENTER_X - 90;
	private static final float INSTRUCTION_Y = GameConstants.SCREEN_CENTER_Y;
	private static final float AUTOWRAP_WIDTH = 500;
	
//	private Category cat;
	private Text instruction;
	private Text title;
	private ButtonSprite playButton;
	private ButtonSprite exitButton;
	private CategoryScene scene;
	private Font titleFont;
	private Font instructionFont;
	
	public InstructionWindow(CategoryScene s){
		super(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y, 
				GameConstants.POPUP_WINDOW_WIDTH, GameConstants.POPUP_WINDOW_HEIGHT, 
				ResourcesManager.getInstance().instruction_region , 
				ResourcesManager.getInstance().vbom);
//		cat = c;
		scene = s;
		createButtons();
		createText();
		this.setScale(0.2f);
	}
	
	private void createButtons(){
		// Play button
		playButton = new ButtonSprite(GameConstants.SCREEN_CENTER_X - 90, 
				GameConstants.SCREEN_CENTER_Y - 180,
				ResourcesManager.getInstance().play_button_region,
				ResourcesManager.getInstance().vbom
				);
		playButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
					float pTouchAreaLocalY) {
				// Start game scene here
				scene.zoom(GameConstants.CAMERA_ZOOM_OUT);
				InstructionWindow.this.disposeWindows();
				SceneManager.getInstance().loadGameScene(ResourcesManager.getInstance().engine,
						ResourcesManager.getInstance().match.getSelectedCategory().getHash());
			}
		});
		playButton.setVisible(true);
		attachChild(playButton);
		SceneManager.getInstance().getCurrentScene().registerTouchArea(playButton);
		
		// Exit button
		exitButton = new ButtonSprite(GameConstants.POPUP_EXIT_BUTTON_X,
				GameConstants.POPUP_EXIT_BUTTON_Y,
				ResourcesManager.getInstance().close_button_region,
				ResourcesManager.getInstance().vbom
				);
		exitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
					float pTouchAreaLocalY) {
				scene.zoom(GameConstants.CAMERA_ZOOM_OUT);
				scene.setOnSceneTouchListener(scene);
				InstructionWindow.this.disposeWindows();
			}
		});
		exitButton.setVisible(true);
		attachChild(exitButton);
		SceneManager.getInstance().getCurrentScene().registerTouchArea(exitButton);
	}
	
	private void createText(){
		FontFactory.setAssetBasePath("font/");
		final ITexture titleTexture = new BitmapTextureAtlas(ResourcesManager.getInstance().activity.getTextureManager(), 
				512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		titleFont = FontFactory.createStrokeFromAsset(ResourcesManager.getInstance().activity.getFontManager(), 
				titleTexture, ResourcesManager.getInstance().activity.getAssets(), "arial.ttf", 50, true, 
				Color.WHITE, 2, Color.parseColor("#c9cb1d"));
		titleFont.load();
		
		final ITexture intructionTexture = new BitmapTextureAtlas(ResourcesManager.getInstance().activity.getTextureManager(), 
				512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		instructionFont = FontFactory.createStrokeFromAsset(ResourcesManager.getInstance().activity.getFontManager(), 
				intructionTexture, ResourcesManager.getInstance().activity.getAssets(), "arial.ttf", 30, true, 
				Color.RED, 2, Color.parseColor("#c9cb1d"));
		instructionFont.load();
		
		title = new Text(TITLE_X, TITLE_Y, titleFont,
				"Chơi như thế nào?", new TextOptions(HorizontalAlign.CENTER), 
				ResourcesManager.getInstance().vbom);
		title.setText("Chơi như thế nào?");
		attachChild(title);
		
		instruction = new Text(INSTRUCTION_X, INSTRUCTION_Y, instructionFont,
				"", 1000,
				new TextOptions(AutoWrap.WORDS, AUTOWRAP_WIDTH, HorizontalAlign.CENTER),
				ResourcesManager.getInstance().vbom);
		instruction.setText(ResourcesManager.getInstance().match.getSelectedCategory().getInstruction());
		Debug.e("InstructionWin", "test:" + instruction.getLineWidthMaximum() + " - " + instruction.getWidth());
		attachChild(instruction);
	}
	
	private void disposeWindows(){
		scene.detachChild(this);
		this.detachSelf();
		title.detachSelf();
		instruction.detachSelf();
		SceneManager.getInstance().getCurrentScene().unregisterTouchArea(playButton);
		SceneManager.getInstance().getCurrentScene().unregisterTouchArea(exitButton);
		playButton = null;
		exitButton = null;
		title = null;
		instruction = null;
		titleFont.unload();
		instructionFont.unload();
	}

	
}
