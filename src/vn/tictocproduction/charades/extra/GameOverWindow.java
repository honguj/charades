package vn.tictocproduction.charades.extra;

import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.debug.Debug;

import android.graphics.Color;
import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.TeamPlayer;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.ResourcesManager;
import vn.tictocproduction.charades.manager.SceneManager;
import vn.tictocproduction.charades.scene.ResultScene;

public class GameOverWindow extends Sprite{
	private static final float TITLE_X = GameConstants.SCREEN_CENTER_X - 80;
	private static final float TITLE_Y = GameConstants.SCREEN_HEIGHT - 130;
	
	private ResultScene scene;
	// UI Elements
	private ButtonSprite backToMenuButton;
	private Font titleFont;
	private Font instructionFont;
	
	public GameOverWindow(BaseScene s){
		super(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y,
				GameConstants.POPUP_WINDOW_WIDTH, GameConstants.POPUP_WINDOW_HEIGHT,
				ResourcesManager.getInstance().gameover_mode_region,
				ResourcesManager.getInstance().vbom);
		scene = (ResultScene)s;
		createButtons();
		createHeader();
		createResults();
	}
	
	private void createButtons(){
		backToMenuButton = new ButtonSprite(GameConstants.SCREEN_CENTER_X - 90, 
				GameConstants.SCREEN_CENTER_Y - 190,
				ResourcesManager.getInstance().back_button_region,
				ResourcesManager.getInstance().vbom);
		backToMenuButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(ButtonSprite pButtonSprite, float pTouchAreaLocalX,
					float pTouchAreaLocalY) {
				disposeWindows();
				// Back to main menu
				SceneManager.getInstance().createMenuScene();
			}
		});
		attachChild(backToMenuButton);
		SceneManager.getInstance().getCurrentScene().registerTouchArea(backToMenuButton);
	}
	
	private void createHeader(){
		FontFactory.setAssetBasePath("font/");
		final ITexture titleTexture = new BitmapTextureAtlas(ResourcesManager.getInstance().activity.getTextureManager(), 
				512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		titleFont = FontFactory.createStrokeFromAsset(ResourcesManager.getInstance().activity.getFontManager(), 
				titleTexture, ResourcesManager.getInstance().activity.getAssets(), "arial.ttf", 50, true, 
				Color.WHITE, 2, Color.parseColor("#c9cb1d"));
		titleFont.load();
		
		final ITexture intructionTexture = new BitmapTextureAtlas(ResourcesManager.getInstance().activity.getTextureManager(), 
				512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		instructionFont = FontFactory.createStrokeFromAsset(ResourcesManager.getInstance().activity.getFontManager(), 
				intructionTexture, ResourcesManager.getInstance().activity.getAssets(), "arial.ttf", 35, true, 
				Color.RED, 2, Color.parseColor("#c9cb1d"));
		instructionFont.load();
		
		Text header = new Text(TITLE_X, TITLE_Y, titleFont,
				ResourcesManager.getInstance().match.getWinner().getName() + " WIN!", 
				new TextOptions(HorizontalAlign.CENTER), 
				ResourcesManager.getInstance().vbom);
		attachChild(header);
	}
	
	private void createResults(){
		int spaceBetweenRows = 70;
		int boxX = GameConstants.SCREEN_CENTER_X - 100;
		int boxY = GameConstants.SCREEN_CENTER_Y + 50;
		
		for (int i = 0; i < ResourcesManager.getInstance().match.getTeams().size(); i++){
			TeamPlayer team = ResourcesManager.getInstance().match.getTeams().get(i);
			Text result = new Text(boxX, boxY, instructionFont, 
					team.getName() +": " + team.getTotalScore(),
					ResourcesManager.getInstance().vbom);
			result.setColor(Color.WHITE);
			attachChild(result);
			boxY -= spaceBetweenRows;
		}
	}
	
	private void disposeWindows(){
		
	}

}
