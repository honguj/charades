package vn.tictocproduction.charades.game;

import java.util.List;

import org.andengine.entity.scene.background.Background;
import org.andengine.util.adt.color.Color;

public class GameConstants {
	// Basic setup
	public static final String SOUND_DIRECTORY = "sound/";
	public static final float GAME_DELAY_INIT = 2.0f;
	public static final int GAME_TIMER_MAX = 5;
	public static final Background GAME_SCENE_BACKGROUND = new Background(51f/255f, 153f/255f, 255f/255f);
	public static final Background GAME_SUCCESS_BACKGROUND = new Background(Color.GREEN);
	public static final Background GAME_FAILED_BACKGROUND = new Background(Color.RED);
	
	public static final int CONNECTION_TIME_OUT_MS = 3000;
	public static final String HOST = "http://tictocprod-idoan.herokuapp.com";
	public static final String CATS = HOST + "/clients";
	public static final String WORDS = HOST + "/clients/";
		
	// Camera
	public static final int SCREEN_CENTER_X = 400;
	public static final int SCREEN_CENTER_Y = 240;
	public static final float SCREEN_WIDTH = 800;
	public static final float SCREEN_HEIGHT = 480;
	public static final float CAMERA_MAX_VELOCITY = 5000;
	public static final float CAMERA_MAX_ZOOM_FACTOR = 5.0f;
	public static final float CAMERA_ZOOM_IN = 5.0f;
	public static final float CAMERA_ZOOM_OUT = 1.0f;

	// Windows popup
	public static final float POPUP_WINDOW_WIDTH = 600;
	public static final float POPUP_WINDOW_HEIGHT = 400;
	public static final float POPUP_EXIT_BUTTON_X = SCREEN_CENTER_X + 180;
	public static final float POPUP_EXIT_BUTTON_Y = SCREEN_CENTER_Y + 150;
	
	// Dual mode
	public static final int[] DUAL_TEAM_CHOICES = {2, 3, 4};
	public static final int[] DUAL_ROUND_CHOICES = {1, 3, 5};
	public static final int DUAL_OPTION_SIZE = 70;
	public static final Color DUAL_SELECTED_COLOR = Color.BLACK;
	public static final Color DUAL_UNSELECTED_COLOR = Color.WHITE;
	
	// Category
	public static final int CATEGORY_COLUMNS_PER_SCREEN = 3;
	public static final int CATEGORY_ROWS_PER_SCREEN = 2;
	public static final int CATEGORY_PADDING_X = 10;
	public static final int CATEGORY_PADDING_Y = 0;
	public static final int CATEGORY_BOX_WIDTH = 200;
	public static final int CATEGORY_BOX_HEIGHT = 200;
	public static final int CATEGORY_SPACES_ROWS = (int) (
			(GameConstants.SCREEN_HEIGHT - GameConstants.CATEGORY_PADDING_Y * 2)
			/ GameConstants.CATEGORY_ROWS_PER_SCREEN 
			- GameConstants.CATEGORY_BOX_HEIGHT
			)/2;
	public static final int CATEGORY_SPACES_COLUMNS = (int) (
			(GameConstants.SCREEN_WIDTH - GameConstants.CATEGORY_PADDING_X*2)
			/ GameConstants.CATEGORY_COLUMNS_PER_SCREEN 
			- GameConstants.CATEGORY_BOX_WIDTH
			) / 2;

	//Result 
	public static final int RESULT_BOX_WIDTH = 400;
	public static final int RESULT_BOX_HEIGHT = 80;
	public static final int RESULT_ROWS_PER_SCREEN = 6;
	public static final int RESULT_PADDING = 5;
	public static final int RESULT_TEXT_SIZE = 50;
	public static final int SCORE_HUD_SIZE = 100;
	public static final int SCORE_HUD_PADDING_Y = 0;
	public static final int SCORE_HUD_X = SCREEN_CENTER_X + 40;
	public static final int SCORE_HUD_Y = (int) (SCREEN_HEIGHT - SCORE_HUD_SIZE/2 - SCORE_HUD_PADDING_Y);
	public static final int NEXT_BUTTON_WIDTH = 130;
	public static final int NEXT_BUTTON_HEIGHT = 100;
	
	// Category loader
	public static final String TAG_CATEGORIES = "categories";
	public static final String TAG_CAT = "category";
	public static final String TAG_CAT_ATTRIBUTE_HASH = "id";
	public static final String TAG_CAT_ATTRIBUTE_INSTRUCTION = "instruction";
	public static final String TAG_ATTRIBUTE_NAME = "name";
	public static final String TAG_WORD = "word";
	
}
