package vn.tictocproduction.charades.scene;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.game.GameActivity;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.SceneManager.SceneType;

public class LoadingScene extends BaseScene{

	@Override
    public void createScene()
    {
        setBackground(new Background(Color.WHITE));
        attachChild(new Text(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y, resourcesManager.font, "Loading...", vbom));
    }

    @Override
    public void onBackKeyPressed()
    {
        return;
    }

    @Override
    public SceneType getSceneType()
    {
        return SceneType.SCENE_LOADING;
    }

    @Override
    public void disposeScene()
    {

    }
}
