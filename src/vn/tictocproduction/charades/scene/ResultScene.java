package vn.tictocproduction.charades.scene;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.ClickDetector;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.input.touch.detector.ClickDetector.IClickDetectorListener;
import org.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.andengine.opengl.util.GLState;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.Result;
import vn.tictocproduction.charades.base.TeamPlayer;
import vn.tictocproduction.charades.extra.GameOverWindow;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.ResourcesManager;
import vn.tictocproduction.charades.manager.SceneManager;
import vn.tictocproduction.charades.manager.SceneManager.SceneType;

public class ResultScene extends BaseScene implements IScrollDetectorListener, IOnSceneTouchListener,
IClickDetectorListener {

	private float lowerBoundary = 0;
	private float upperBoundary = 0;
	private float mCurrentY = 0;

	private TeamPlayer player;
	private HUD scoreHUD;

	// Scrolling
	private SurfaceScrollDetector mScrollDetector;
	private ClickDetector mClickDetector;

	public ResultScene(TeamPlayer p){
		super();

		player = p;
		Debug.e("ResultScene", "result:" + player.getResults().size());
		createScrollableMenu();
		lowerBoundary = (float) (GameConstants.SCREEN_HEIGHT
				- GameConstants.RESULT_BOX_HEIGHT * player.getResults().size())
				- GameConstants.SCORE_HUD_SIZE - GameConstants.SCORE_HUD_PADDING_Y;
	}

	@Override
	public void createScene() {
		createBackground();			
	}

	private void createScrollableMenu() {
		mScrollDetector = new SurfaceScrollDetector(this);
		mClickDetector = new ClickDetector(this);
		setOnSceneTouchListener(this);
		setTouchAreaBindingOnActionDownEnabled(true);
		setTouchAreaBindingOnActionMoveEnabled(true);
		setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		createResultPanel();
		createScoreHUD();
	}

	private void createScoreHUD() {
		scoreHUD = new HUD();

		// Background for HUD
		scoreHUD.attachChild(new Sprite(GameConstants.SCREEN_CENTER_X, GameConstants.SCORE_HUD_Y, resourcesManager.result_HUD_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		});

		// Score text in HUD
		Text scoreText = new Text(GameConstants.SCORE_HUD_X - 20, GameConstants.SCORE_HUD_Y, 
				resourcesManager.resultFont, "Score: 0123456789", new TextOptions(HorizontalAlign.LEFT), vbom);
		scoreText.setText(player.getScore() + "");
		scoreHUD.attachChild(scoreText);

		// Next button in HUD - Only appears in Dual Mode
		if (ResourcesManager.getInstance().match.getTeams().size() > 1){
			ButtonSprite nextButton = new ButtonSprite(
					GameConstants.SCREEN_WIDTH - GameConstants.NEXT_BUTTON_WIDTH/2, 
					GameConstants.SCREEN_HEIGHT - GameConstants.NEXT_BUTTON_HEIGHT/2, 
					ResourcesManager.getInstance().next_button_region, 
					vbom){
				@Override
				public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
					Debug.e("ResultScene", "Round:" + ResourcesManager.getInstance().match.getCurrentRound()
							+ " - CurrentTeamId:" + ResourcesManager.getInstance().match.getCurrentTeam().getName());
					// Go to next round in dual mode
					if (ResourcesManager.getInstance().match.isOver()){
						// Load game over scene
						Debug.e("ResultScene", "GameOver");
						camera.setHUD(null);
						GameOverWindow w = new GameOverWindow(ResultScene.this);
						ResultScene.this.attachChild(w);
					}
					else if(ResourcesManager.getInstance().match.roundEnded()){
						Debug.e("ResultScene", "NextRound");
						// Load category scene for next round
						ResourcesManager.getInstance().match.nextRound();
						SceneManager.getInstance().loadCategoryScene(ResourcesManager.getInstance().engine);
					}
					else {
						Debug.e("ResultScene", "NextTeam");
						// Load game scene for next team
						ResourcesManager.getInstance().match.nextTeam();
						// Load Game scene for next round
						SceneManager.getInstance().loadGameScene(ResourcesManager.getInstance().engine,
								ResourcesManager.getInstance().match.getSelectedCategory().getHash());
					}
					return false;
				}
			};
			scoreHUD.attachChild(nextButton);
			registerTouchArea(nextButton);
		}
		camera.setHUD(scoreHUD);
	}

	private void createResultPanel() {
		// Calculate space between each level square
		int spaceBetweenRows = (int) ((GameConstants.SCREEN_HEIGHT / GameConstants.RESULT_ROWS_PER_SCREEN) - GameConstants.RESULT_PADDING);

		int boxX = GameConstants.RESULT_BOX_WIDTH;
		int boxY = (int) (GameConstants.SCREEN_HEIGHT - GameConstants.RESULT_BOX_HEIGHT/2
				- GameConstants.SCORE_HUD_SIZE - GameConstants.SCORE_HUD_PADDING_Y);

		for (int y = 0; y < player.getResults().size(); y++) {
			Rectangle box = new Rectangle(boxX, boxY, GameConstants.RESULT_BOX_WIDTH, GameConstants.RESULT_BOX_HEIGHT, vbom) {
				@Override
				public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
					return false;
				}
			};
			box.setColor(Color.WHITE);
			attachChild(box);

			//Center for different font size
			Text wordText = new Text(boxX + 18, boxY + 15, resourcesManager.resultFont, player.getResults().get(y).getWord(), vbom);
			if (player.getResults().get(y).getResult()){
				wordText.setColor(Color.BLACK);
			}
			else{
				wordText.setColor(Color.RED);
				wordText.setAlpha(0.5f);
			}
			attachChild(wordText);
//			registerTouchArea(box);
			boxY -= spaceBetweenRows + GameConstants.RESULT_PADDING;
		}
	}

	@Override
	public void onBackKeyPressed() {
		// TODO this is a hack to temporary return to menu scene - Need to add back button
		//		SceneManager.getInstance().reloadMenuScene(engine);
		SceneManager.getInstance().createMenuScene();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAMEOVER;
	}

	@Override
	public void disposeScene() {
		camera.setHUD(null);
		camera.setCenter(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y);
		camera.offsetCenter(0,0);
	}

	private void createBackground(){
		setBackground(new Background(Color.WHITE));
	}

	@Override
	public void onScrollStarted(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
	}

	@Override
	public void onScroll(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {

		if ((mCurrentY + pDistanceY < lowerBoundary) || (mCurrentY + pDistanceY > upperBoundary) )
			return;
		camera.offsetCenter(0, pDistanceY);
		mCurrentY += pDistanceY;  
	}

	@Override
	public void onScrollFinished(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		mClickDetector.onTouchEvent(pSceneTouchEvent);
		mScrollDetector.onTouchEvent(pSceneTouchEvent);
		return true;
	}

	@Override
	public void onClick(ClickDetector pClickDetector, int pPointerID,
			float pSceneX, float pSceneY) {

	}
}
