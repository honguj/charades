package vn.tictocproduction.charades.scene;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.*;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.Result;
import vn.tictocproduction.charades.base.TeamPlayer;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.CategoryManager;
import vn.tictocproduction.charades.manager.ResourcesManager;
import vn.tictocproduction.charades.manager.SceneManager;
import vn.tictocproduction.charades.manager.SceneManager.SceneType;

public class GameScene extends BaseScene implements IOnSceneTouchListener, SensorEventListener{ 

	private List<String> wordsList;
	private String guessingWord;
	private TeamPlayer player;

	// Sensor
	private float[] userReferences = {0f, 0f, -1f};

	private float[] mLastAccelerometer = new float[3];
	private float[] mLastMagnetometer = new float[3];
	private boolean mLastAccelerometerSet = false;
	private boolean mLastMagnetometerSet = false;

	private float[] mR = new float[9];
	private float[] mIncline = new float[9];
	private float[] mOrientation = new float[3];
	private SensorManager sensorManager;

	// HUD
	private HUD gameHUD;
	private int timerCount = GameConstants.GAME_TIMER_MAX;
	private Text guessingWordText;
	private Text timerText;

	private boolean gameStarted = false;
	private boolean newWord = false;

	private TimerHandler timeHandler;
	private TimerHandler changeScreenTimer;
	private TimerHandler timerEnd;

	public GameScene(String catId, TeamPlayer p){
		super();
		player = p;
		wordsList = CategoryManager.getInstance().loadWordList(catId, this);
		Debug.e("GameScene", "words size:" + wordsList.size());
	}

	@Override
	public void createScene() {
		createBackground();
		createHUD();
		setOnSceneTouchListener(this);
	}

	private void initSensor() {
		sensorManager = (SensorManager)activity.getSystemService(activity.SENSOR_SERVICE);
		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 
				sensorManager.SENSOR_DELAY_GAME);
		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), 
				sensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	public void disposeScene() {
		camera.setHUD(null);
		camera.setChaseEntity(null);
		if (sensorManager != null){
			sensorManager.unregisterListener(this);
		}
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_GAME;
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if (!gameStarted){
			initSensor();
			gameStarted = true;
			createTimeHandler();
			generateNewWords();
		}

		return false;
	}

	private void gameOver(){
		guessingWordText.setText("Time's up");
		setBackground(GameConstants.GAME_FAILED_BACKGROUND);
		timerEnd = new TimerHandler(1.0f, new ITimerCallback(){
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				engine.unregisterUpdateHandler(changeScreenTimer);
				engine.unregisterUpdateHandler(timeHandler);
				engine.unregisterUpdateHandler(timerEnd);

				// TODO to remove
				for (int i = 0; i< player.getResults().size(); i++) {
					System.out.println("word=" + player.getResults().get(i).getWord() + 
							", value=" + player.getResults().get(i).getResult());
				}

				SceneManager.getInstance().loadResultScene(engine, player.getScore(), player.getResults());
			}
		});
		engine.registerUpdateHandler(timerEnd);
	}

	private void generateNewWords() {
		int rand = (int)(Math.random() * ((wordsList.size())));
		guessingWord = wordsList.get(rand);
		guessingWordText.setText(guessingWord);
		wordsList.remove(guessingWord);
		//		Debug.e("GameScene", "generateNewWords end: " + wordsList.size());
	}

	@Override
	public void onBackKeyPressed() {
//		SceneManager.getInstance().loadCategoryScene(engine);
		SceneManager.getInstance().createMenuScene();
	}

	////////////////////////////////////////
	/// Private function
	////////////////////////////////////////
	private void createBackground()
	{
		setBackground(GameConstants.GAME_SCENE_BACKGROUND);
	}

	private void createHUD()
	{
		guessingWordText = new Text(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y,
				resourcesManager.font, "", 300, new TextOptions(AutoWrap.WORDS, 500, HorizontalAlign.CENTER), vbom);
		String s = "Round " + ResourcesManager.getInstance().match.getCurrentRound() + " - " +
				ResourcesManager.getInstance().match.getCurrentTeam().getName() + " \n Touch to start";
		guessingWordText.setText(s);
		guessingWordText.setColor(Color.YELLOW);
		guessingWordText.setVisible(true);
		attachChild(guessingWordText);

		timerText = new Text(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_HEIGHT - 100,
				resourcesManager.font, "30", new TextOptions(HorizontalAlign.CENTER), vbom);
		timerText.setText(timerCount+"");
		timerText.setColor(Color.YELLOW);
		timerText.setVisible(true);
		attachChild(timerText);	
	}

	private void createTimeHandler(){
		timeHandler = new TimerHandler(1.0f, new ITimerCallback(){
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				timeHandler = pTimerHandler;
				timeHandler.reset();
				if (newWord)
					newWord = false;
				timerCount--;
				timerText.setText(timerCount+"");
				if (timerCount == 0){
					gameOver();
					//engine.unregisterUpdateHandler(timeHandler);
				}
			}
		});
		engine.registerUpdateHandler(timeHandler);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		synchronized (this) {
			switch (event.sensor.getType()){
			case Sensor.TYPE_ACCELEROMETER:
				if (mLastAccelerometer == null)
					mLastAccelerometer = new float[3];
				System.arraycopy(event.values, 0, mLastAccelerometer, 0, event.values.length);
				mLastAccelerometerSet = true;
				break;
			case Sensor.TYPE_MAGNETIC_FIELD:
				if (mLastMagnetometer == null)
					mLastMagnetometer = new float[3];
				System.arraycopy(event.values, 0, mLastMagnetometer, 0, event.values.length);
				mLastMagnetometerSet = true;
				break;
			}

			if (mLastAccelerometerSet && mLastMagnetometerSet) {
				SensorManager.getRotationMatrix(mR, mIncline, mLastAccelerometer, mLastMagnetometer);
				SensorManager.getOrientation(mR, mOrientation);

				int inclination = (int)(mOrientation[2] - userReferences[2]);
				//	            Debug.e("Orientation", "" + inclination);
				if ( wordsList.size() > 0 && !newWord && timerCount > 0 && guessingWord != null){
					if (inclination == 3){
						newWord = true;
						player.addResult(new Result(guessingWord, true));
						setBackground(GameConstants.GAME_SUCCESS_BACKGROUND);
						guessingWordText.setText("CORRECT");
						//						Debug.e("GameScene", "word:" + guessingWord + " - score: " + score + " - results:" + wordsResult.size() );
						changeScreenTimer = new TimerHandler(0.5f, new ITimerCallback(){
							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								setBackground(GameConstants.GAME_SCENE_BACKGROUND);
								generateNewWords();
								engine.unregisterUpdateHandler(changeScreenTimer);
							}
						});
						engine.registerUpdateHandler(changeScreenTimer);
					}
					else if (inclination == 1){
						newWord = true;
						player.addResult(new Result(guessingWord, false));
						setBackground(GameConstants.GAME_FAILED_BACKGROUND);
						guessingWordText.setText("PASS");
						//						Debug.e("GameScene", "word:" + guessingWord + " - FAILED - score: " + score + " - results:" + wordsResult.size() );
						changeScreenTimer = new TimerHandler(0.5f, new ITimerCallback(){
							@Override
							public void onTimePassed(TimerHandler pTimerHandler) {
								setBackground(GameConstants.GAME_SCENE_BACKGROUND);
								generateNewWords();
								engine.unregisterUpdateHandler(changeScreenTimer);
							}
						});
						engine.registerUpdateHandler(changeScreenTimer);	            	
					}
				}

			}
		}
	}


}
