package vn.tictocproduction.charades.scene;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.background.ParallaxBackground.ParallaxEntity;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.ButtonSprite.OnClickListener;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.ClickDetector;
import org.andengine.input.touch.detector.ClickDetector.IClickDetectorListener;
import org.andengine.input.touch.detector.ScrollDetector;
import org.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.andengine.input.touch.detector.SurfaceScrollDetector;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.util.SAXUtils;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;
import org.andengine.util.level.EntityLoader;
import org.andengine.util.level.constants.LevelConstants;
import org.andengine.util.level.simple.SimpleLevelEntityLoaderData;
import org.andengine.util.level.simple.SimpleLevelLoader;
import org.xml.sax.Attributes;

import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.Category;
import vn.tictocproduction.charades.extra.InstructionWindow;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.CategoryManager;
import vn.tictocproduction.charades.manager.ResourcesManager;
import vn.tictocproduction.charades.manager.SceneManager;
import vn.tictocproduction.charades.manager.SceneManager.SceneType;
import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.GpsStatus.NmeaListener;

public class CategoryScene extends BaseScene implements IScrollDetectorListener, 
IOnSceneTouchListener, IClickDetectorListener {

	private Category selectedCat = null;
	private float lowerBoundary = 0;
	private float upperBoundary = 0;
	private float mCurrentY = 0;
	private List<Category> categories;

	// Scrolling
	private SurfaceScrollDetector mScrollDetector;
	private ClickDetector mClickDetector;
	private boolean isScrolling = false;

	@Override
	public void createScene() {
		categories = CategoryManager.getInstance().loadAllCategories(this);
		createBackground();
		createScrollableMenu();
	}

	private void createScrollableMenu() {
		mScrollDetector = new SurfaceScrollDetector(this);
		mClickDetector = new ClickDetector(this);
		setOnSceneTouchListener(this);
		setTouchAreaBindingOnActionDownEnabled(true);
		setTouchAreaBindingOnActionMoveEnabled(true);
		setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		createCategoryBoxes();
	}

	private void createCategoryBoxes() {
		// calculate the amount of required columns for the level count
		int totalRows = (categories.size() / GameConstants.CATEGORY_COLUMNS_PER_SCREEN) + 1;

		//Current Level Counter
		int id = 0;

		//Create the Level selectors, one row at a time.
		int boxX = GameConstants.CATEGORY_BOX_WIDTH/2 + GameConstants.CATEGORY_PADDING_X
				+ GameConstants.CATEGORY_SPACES_COLUMNS;
		int boxY = (int) (GameConstants.SCREEN_HEIGHT - GameConstants.CATEGORY_BOX_HEIGHT/2 
				- GameConstants.CATEGORY_SPACES_ROWS - GameConstants.CATEGORY_PADDING_Y);
		for (int y = 0; y < totalRows; y++) {
			for (int x = 0; x < GameConstants.CATEGORY_COLUMNS_PER_SCREEN; x++) {
				//On Touch, save the clicked level in case it's a click and not a scroll.
				final Category catToLoad = categories.get(id);

				Rectangle box = new Rectangle(boxX, boxY, GameConstants.CATEGORY_BOX_WIDTH, GameConstants.CATEGORY_BOX_HEIGHT, vbom) {
					@Override
					public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
						selectedCat = catToLoad;
						Debug.e("Category box","offset: " +this.getOffsetCenterX() + " - " + this.getOffsetCenterY());
						return false;
					}
				};

				box.setColor(Color.RED);

				attachChild(box);

				//Center for different font size
				if (id < 10) {
					attachChild(new Text(boxX + 18, boxY + 15, resourcesManager.font, categories.get(id).getName(), vbom));
				}
				else {
					attachChild(new Text(boxX + 10, boxY + 15, resourcesManager.font, categories.get(id).getName(), vbom));
				}

				registerTouchArea(box);

				id++;
				boxX += GameConstants.CATEGORY_SPACES_COLUMNS * 2 + GameConstants.CATEGORY_BOX_WIDTH;

				if (id >= categories.size())
					break;
			}

			if (id >= categories.size())
				break;

			boxY -= GameConstants.CATEGORY_SPACES_ROWS * 2 + GameConstants.CATEGORY_BOX_HEIGHT;
			boxX = GameConstants.CATEGORY_BOX_WIDTH/2 + GameConstants.CATEGORY_PADDING_X + GameConstants.CATEGORY_SPACES_COLUMNS;
		}
	}

	@Override
	public void onBackKeyPressed() {
//		SceneManager.getInstance().reloadMenuScene(engine);
		SceneManager.getInstance().createMenuScene();
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_CATEGORY;
	}

	@Override
	public void disposeScene() {
		mCurrentY = 0;
		camera.offsetCenter(0,0);
		camera.setCenter(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y);
	}

	private void createBackground(){
		setBackground(GameConstants.GAME_SCENE_BACKGROUND);
	}

	@Override
	public void onClick(ClickDetector pClickDetector, int pPointerID,
			float pSceneX, float pSceneY) {
		if (isScrolling)
			return;
		if (selectedCat == null )
			return;
		else
			loadLevel(selectedCat);
	}

	private void loadLevel(Category cat) {
		if (cat == null){
			return;
		}
		
		// Set selected category for the current match
		ResourcesManager.getInstance().match.setSelectedCategory(cat);
		disposeScene();
		// Display instruction windows
		InstructionWindow w = new InstructionWindow(this);
		attachChild(w);
		// Temporary unregister onTouchListener on scene
		setOnSceneTouchListener(null);
		// Zoom effect
		zoom(GameConstants.CAMERA_ZOOM_IN);
	}

	public void zoom(float f){
		((SmoothCamera)camera).setZoomFactor(f);
		TimerHandler tmp = new TimerHandler(2.0f, new ITimerCallback() {
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {		
				engine.unregisterUpdateHandler(pTimerHandler);
			}
		});
		engine.registerUpdateHandler(tmp);
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		this.mClickDetector.onTouchEvent(pSceneTouchEvent);
		this.mScrollDetector.onTouchEvent(pSceneTouchEvent);
		return true;
	}

	@Override
	public void onScrollStarted(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
	}

	@Override
	public void onScroll(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
		isScrolling = true;
		int totalRows = (categories.size() / GameConstants.CATEGORY_COLUMNS_PER_SCREEN) + 1;
		lowerBoundary = (float) (GameConstants.SCREEN_HEIGHT
				- GameConstants.CATEGORY_BOX_HEIGHT 
				* Math.ceil(categories.size()*1.0/GameConstants.CATEGORY_COLUMNS_PER_SCREEN)
				- GameConstants.CATEGORY_PADDING_Y*2
				- GameConstants.CATEGORY_SPACES_ROWS * 2 * totalRows);
		//		Debug.e("onScroll","lowerBound:" + lowerBoundary + " - UpperBound:" + upperBoundary);
		if ((mCurrentY + pDistanceY < lowerBoundary) || (mCurrentY + pDistanceY > upperBoundary) )
			return;
		camera.offsetCenter(0, pDistanceY);
		//		camera.setCenter(GameConstants.SCREEN_CENTER_X, pDistanceY);
		mCurrentY += pDistanceY;  
	}

	@Override
	public void onScrollFinished(ScrollDetector pScollDetector, int pPointerID,
			float pDistanceX, float pDistanceY) {
		isScrolling = false;
	}
}
