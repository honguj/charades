package vn.tictocproduction.charades.scene;

import java.util.ArrayList;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.Match;
import vn.tictocproduction.charades.base.TeamPlayer;
import vn.tictocproduction.charades.extra.DualModeWindow;
import vn.tictocproduction.charades.game.GameActivity;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.ResourcesManager;
import vn.tictocproduction.charades.manager.SceneManager;
import vn.tictocproduction.charades.manager.SceneManager.SceneType;

public class MainMenuScene extends BaseScene implements IOnMenuItemClickListener{

	private MenuScene menuChildScene;
	private final int MENU_QUICKPLAY = 0;
	private final int MENU_DUAL = 1;
	
	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		switch(pMenuItem.getID())
		{
		case MENU_QUICKPLAY:
			TeamPlayer team = new TeamPlayer("QuickPlay player");
			ResourcesManager.getInstance().match = new Match(1);
			ResourcesManager.getInstance().match.addTeam(team);
			
			SceneManager.getInstance().loadCategoryScene(engine);
			return true;
		case MENU_DUAL:
			// Remove menu elements
			menuChildScene.clearMenuItems();
			// Unregister on click listener
			menuChildScene.setOnMenuItemClickListener(null);
			
			// Display dual option windows
			DualModeWindow w = new DualModeWindow(this);
			attachChild(w);
			
			return true;
		default:
			return false;
		}
	}

	@Override
	public void createScene() {
		createBackground();
		createMenuChildScene();
	}

	@Override
	public void onBackKeyPressed() {
		System.exit(0);
	}

	@Override
	public SceneType getSceneType() {
		return SceneType.SCENE_MENU;
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}
	
	private void createBackground(){
		attachChild(new Sprite(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y, resourcesManager.menu_background_region, vbom)
		{
			@Override
			protected void preDraw(GLState pGLState, Camera pCamera) 
			{
				super.preDraw(pGLState, pCamera);
				pGLState.enableDither();
			}
		});
	}

	private void createMenuChildScene(){
		menuChildScene = new MenuScene(camera);
	    menuChildScene.setPosition(0,-70);	    
	    
	    createMenuItems();
	    
	    menuChildScene.setOnMenuItemClickListener(this);
	    setChildScene(menuChildScene);
	    
	}
	
	public void createMenuItems(){
		final IMenuItem actItOutMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_QUICKPLAY, 
	    		resourcesManager.actItOut_region, vbom), 1.2f, 1);
	    menuChildScene.addMenuItem(actItOutMenuItem);
	    menuChildScene.buildAnimations();
	    menuChildScene.setBackgroundEnabled(false);
	    actItOutMenuItem.setPosition(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y);
	    
	    final IMenuItem tabouMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_DUAL, 
	    		resourcesManager.tabou_region, vbom), 1.2f, 1);
	    menuChildScene.addMenuItem(tabouMenuItem );
	    menuChildScene.buildAnimations();
	    menuChildScene.setBackgroundEnabled(false);
	    tabouMenuItem.setPosition(GameConstants.SCREEN_CENTER_X, GameConstants.SCREEN_CENTER_Y - 70);//tabouMenuItem.getX(), tabouMenuItem.getY() - 10);
	}
	
	public MenuScene getMenuScene() { return menuChildScene; }
	
}
