package vn.tictocproduction.charades.base;

import java.util.ArrayList;
import java.util.List;

public class Category {

	private String name;
	private String hash; // TODO use it for security connection
	private String instruction;
	private boolean isLocked;
	private List<String> words;
	
	public Category(String name, String hash, String i){
		this.name = name;
		this.hash = hash;
		instruction = i;
		isLocked = true;
		words = new ArrayList<String>();
	}
	
	public Category(String name, String hash, String i, boolean locked){
		this.name = name;
		this.hash = hash;
		instruction = i;
		words = new ArrayList<String>();
		isLocked = locked;
	}
	
	// Getters and setters
	public boolean isLocked() { return isLocked; }
	public List<String> getWordsList() { return words; }
	public String getName() { return name; }
	public String getHash() { return hash; }
	public String getInstruction() { return instruction; }
	
	public void setLocked(boolean locked) { isLocked = locked; }
	public void setName(String n) { name = n; }
	public void setHash(String h) { hash = h; }
	public void setInstruction(String i) { instruction = i; }
	public void setWordsList(List<String> words) { this.words = words; }
	
}
