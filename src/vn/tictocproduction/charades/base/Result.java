package vn.tictocproduction.charades.base;

public class Result {
	private String word;
	private boolean result;
	
	public Result(String w, boolean r){ word = w; result = r; }
	
	public String getWord() { return word; }
	public boolean getResult() { return result; }

}
