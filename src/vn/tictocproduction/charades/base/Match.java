package vn.tictocproduction.charades.base;

import java.util.ArrayList;
import java.util.List;

import org.andengine.util.debug.Debug;

public class Match {

	private int totalRounds;
	private List<TeamPlayer> teams;
	private int currentRound = 1;
	private int currentTeamId = 0;
	private Category selectedCat;
	
	public Match(int r){
		totalRounds = r;
		teams = new ArrayList<TeamPlayer>();
	}
	
	public Match(int r, List<TeamPlayer> t) {
		totalRounds = r;
		teams = t;
	}
	
	public int getTotalRounds() { return totalRounds; }
	public List<TeamPlayer> getTeams() { return teams; }
	public int getCurrentRound() { return currentRound; }
	public Category getSelectedCategory() { return selectedCat; }
	public TeamPlayer getCurrentTeam() { return teams.get(currentTeamId); }
	public boolean isOver() { return roundEnded() && currentRound == totalRounds; }
	public boolean roundEnded() { return currentTeamId == teams.size()-1; } 
	
	public void addTeam(TeamPlayer t){ teams.add(t); }
	public void setSelectedCategory(Category c) { selectedCat = c; }
	public void nextRound() { 
		currentRound++;
		currentTeamId = 0;
		for(int i=0; i < teams.size(); i++){
			// Cumulate the current team's score and reset result list
//			teams.get(i).cumulateScore();
			teams.get(i).resetResult();	
		}
	}
	
	public void nextTeam(){ 
		// Go to next team
		currentTeamId++;
	}
	
	public TeamPlayer getWinner(){
		TeamPlayer winner = teams.get(0);
		for (int i = 1; i < teams.size(); i++){
			if (teams.get(i).getTotalScore() > winner.getTotalScore())
				winner = teams.get(i);
		}
		return winner;
	}
}
