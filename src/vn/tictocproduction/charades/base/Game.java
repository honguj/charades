package vn.tictocproduction.charades.base;

import java.util.List;

public class Game {
	private int rounds;
	private List<TeamPlayer> teams;
	
	public Game(int r, List<TeamPlayer> t) {
		rounds = r;
		teams = t;
	}
	
	// SETTERS
	public int getRounds() { return rounds; }
	public List<TeamPlayer> getTeams() { return teams; }
	
}
