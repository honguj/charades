package vn.tictocproduction.charades.base;

import java.util.ArrayList;
import java.util.List;

import org.andengine.util.debug.Debug;

public class TeamPlayer {
	private String name;
	private List<Result> results;
	private int score;
	private int totalScore;
	
	public TeamPlayer(String n) { 
		name = n;
		results = new ArrayList<Result>();
		score = 0;
		totalScore = 0;
	}
	
	// GETTERS
	public String getName() { return name; }
	public List<Result> getResults() { return results; }
	public int getScore() { return score; }
	public int getTotalScore() { return totalScore; }
	
	// SETTERS
//	public void setScore(int s) { score = s; }
	public void addResult(Result r) { 
		results.add(r); 
		if (r.getResult()){
			score++;
			totalScore++;
		}
	}
	
	public void cumulateScore() {
		totalScore += score;
		Debug.e("Player", "total:" + totalScore);
	}
	
	public void resetResult(){
		results.clear();
		score = 0;
	}
	
}
