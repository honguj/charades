package vn.tictocproduction.charades.manager;

import java.util.List;
import java.util.Map;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.ui.IGameInterface.OnCreateSceneCallback;
import org.andengine.util.debug.Debug;

import vn.tictocproduction.charades.scene.*;
import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.Result;
import vn.tictocproduction.charades.base.TeamPlayer;
import vn.tictocproduction.charades.manager.ResourcesManager;

public class SceneManager {
	public enum SceneType
	{
		SCENE_SPLASH,
		SCENE_MENU,
		SCENE_CATEGORY,
		SCENE_GAME,
		SCENE_LOADING,
		SCENE_GAMEOVER,
	}

	//---------------------------------------------
	// SCENES
	//---------------------------------------------
	private BaseScene splashScene;
	private BaseScene menuScene;
	private BaseScene categoryScene;
	private BaseScene gameScene;
	private BaseScene loadingScene;
	private BaseScene resultScene;

	//---------------------------------------------
	// VARIABLES
	//---------------------------------------------

	private static final SceneManager INSTANCE = new SceneManager();

	private SceneType currentSceneType = SceneType.SCENE_SPLASH;

	private BaseScene currentScene;

	private Engine engine = ResourcesManager.getInstance().engine;

	//---------------------------------------------
	// GETTERS AND SETTERS
	//---------------------------------------------

	public static SceneManager getInstance()
	{
		return INSTANCE;
	}

	public SceneType getCurrentSceneType()
	{
		return currentSceneType;
	}

	public BaseScene getCurrentScene()
	{
		return currentScene;
	}

	public void setScene(BaseScene scene)
	{
		engine.setScene(scene);
		currentScene = scene;
		currentSceneType = scene.getSceneType();
	}

	public void setScene(SceneType sceneType)
	{
		switch (sceneType)
		{
		case SCENE_MENU:
			setScene(menuScene);
			break;
		case SCENE_CATEGORY:
			setScene(categoryScene);
			break;
		case SCENE_SPLASH:
			setScene(splashScene);
			break;
		case SCENE_LOADING:
			setScene(loadingScene);
			break;
		case SCENE_GAMEOVER:
			setScene(resultScene);
			break;
		default:
			break;
		}
	}

	//-------------------------- SPLASH SCENE ---------------------------------------------
	public void createSplashScene(OnCreateSceneCallback pOnCreateSceneCallback)
	{
		ResourcesManager.getInstance().loadSplashScreen();
		splashScene = new SplashScene();
		currentScene = splashScene;
		pOnCreateSceneCallback.onCreateSceneFinished(splashScene);
	}

	private void disposeSplashScene()
	{
		ResourcesManager.getInstance().unloadSplashScreen();
		splashScene.disposeScene();
		splashScene = null;
	}

	//-------------------------- END SPLASH SCENE ---------------------------------------------

	//-------------------------- MENU SCENE ---------------------------------------------
	/**
	 * Load menu scene for the first time
	 */
	public void createMenuScene()
	{
		ResourcesManager.getInstance().loadMenuResources();
		menuScene = new MainMenuScene();
		loadingScene = new LoadingScene();
		setScene(menuScene);
		if(splashScene != null)
			disposeSplashScene();
		if(resultScene != null)
			disposeResultScene();
	}

	public void disposeMenuScene(){
		ResourcesManager.getInstance().unloadMenuTextures();

		menuScene.disposeScene();
		menuScene = null;
	}

	/**
	 * Reload menu scene when user try to go back to menu from game scene
	 * @param mEngine
	 */
//	public void reloadMenuScene(final Engine mEngine)
//	{
//		setScene(loadingScene);
//		if (categoryScene != null)
//			disposeCategoryScene();
//		if (resultScene != null)
//			disposeResultScene();
//		ResourcesManager.getInstance().unloadGameTextures();
//		mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() 
//		{
//			public void onTimePassed(final TimerHandler pTimerHandler) 
//			{
//				mEngine.unregisterUpdateHandler(pTimerHandler);
//				ResourcesManager.getInstance().loadMenuTextures();
//				setScene(menuScene);
//			}
//		}));
//	}
	//-------------------------- END MENU SCENE ---------------------------------------------


	//-------------------------- CATEGORY SCENE ---------------------------------------------
	public void loadCategoryScene(final Engine mEngine)
	{
		setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() 
		{
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadCategoryResources();
				categoryScene = new CategoryScene();
				setScene(categoryScene);
				if (menuScene != null)
					disposeMenuScene();
				if (resultScene!=null)
					disposeResultScene();
			}
		}));
	}

	public void disposeCategoryScene(){
		ResourcesManager.getInstance().unloadCategoryTextures();
		
		categoryScene.disposeScene();
		categoryScene = null;
	}

	//-------------------------- END CATEGORY SCENE ---------------------------------------------

	//-------------------------- GAME SCENE ---------------------------------------------
	public void loadGameScene(final Engine mEngine, final String catId)
	{
		setScene(loadingScene);
		ResourcesManager.getInstance().unloadMenuTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() 
		{
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadGameResources();
				
				gameScene = new GameScene(catId, ResourcesManager.getInstance().match.getCurrentTeam());
				setScene(gameScene);
				if (categoryScene != null)
					disposeCategoryScene();
				if (resultScene != null)
					disposeResultScene();
			}
		}));
	}
	
	public void disposeGameScene(){
		ResourcesManager.getInstance().unloadGameTextures();
		
//		gameScene.disposeScene();
//		gameScene = null;
	}
	//-------------------------- END GAME SCENE ---------------------------------------------
	
	//-------------------------- RESULT SCENE ---------------------------------------------
	public void loadResultScene(final Engine mEngine, final int score, final List<Result> wordsResult){
		setScene(loadingScene);
		ResourcesManager.getInstance().unloadGameTextures();
		mEngine.registerUpdateHandler(new TimerHandler(0.1f, new ITimerCallback() {
			
			@Override
			public void onTimePassed(TimerHandler pTimerHandler) {
				mEngine.unregisterUpdateHandler(pTimerHandler);
				ResourcesManager.getInstance().loadResultResources();
				resultScene = new ResultScene(ResourcesManager.getInstance().match.getCurrentTeam());
				setScene(resultScene);
				disposeGameScene();
			}
		}));
	}
	
	public void disposeResultScene(){
		resultScene.disposeScene();
		resultScene = null;
	}
	//-------------------------- END RESULT SCENE ---------------------------------------------
}
