package vn.tictocproduction.charades.manager;

import java.io.IOException;

import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.bitmap.AssetBitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import android.graphics.Color;
import vn.tictocproduction.charades.base.Match;
import vn.tictocproduction.charades.base.TeamPlayer;
import vn.tictocproduction.charades.game.GameActivity;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.manager.ResourcesManager;

public class ResourcesManager {
	private static final ResourcesManager INSTANCE = new ResourcesManager();

	public Engine engine;
	public GameActivity activity;
	public Camera camera;
	public VertexBufferObjectManager vbom;
	public Match match;
	//---------------------------------------------
	// TEXTURES & TEXTURE REGIONS
	//---------------------------------------------
	public ITextureRegion splash_region;
	private BitmapTextureAtlas splashTextureAtlas;

	// Menu scene
	public ITextureRegion menu_background_region;
	public ITextureRegion actItOut_region;
	public ITextureRegion tabou_region;
	public ITextureRegion dual_mode_region;
	public ITextureRegion dual_play_button_region;
	public ITextureRegion dual_close_button_region;
	private BuildableBitmapTextureAtlas menuTextureAtlas;
	public Font font;
	
	// Category scene
	public ITextureRegion category_background_region;
	public ITextureRegion cat_region;
	public ITextureRegion instruction_region;
	public ITextureRegion play_button_region;
	public ITextureRegion close_button_region;
	private BuildableBitmapTextureAtlas categoryTextureAtlas;
	
	// Result scene
	public Font resultFont;
	public ITextureRegion result_HUD_region;
	public ITextureRegion next_button_region;
	public ITextureRegion gameover_mode_region;
	public ITextureRegion back_button_region;
	private BuildableBitmapTextureAtlas resultTextureAtlas;

	// ----------------------- Splash screen -------------------------------------
	public void loadSplashScreen(){
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 400, 400, TextureOptions.BILINEAR);
		splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "splash.png", 0, 0);
		splashTextureAtlas.load();
		Debug.e("ResourcesManager","loadPlashScreen done");
	}

	public void unloadSplashScreen(){
		splashTextureAtlas.unload();
		splash_region = null;
	}
	// ----------------------- End Splash screen -------------------------------------

	// ----------------------- Menu screen -------------------------------------
	public void loadMenuResources(){
		loadMenuGraphics();
		loadMenuAudio();
		loadMenuFonts();
	}

	public void loadMenuTextures(){
		menuTextureAtlas.load();
	}

	public void unloadMenuTextures(){
		menuTextureAtlas.unload();
	}

	private void loadMenuGraphics(){
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		menuTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
		menu_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "background.png");
		actItOut_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "actItOutButton.png");
		tabou_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "tabouButton.png");
		dual_mode_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "InstructionBg.png");
		dual_play_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "playButton.png");
		dual_close_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuTextureAtlas, activity, "closeButton.png");
		try 
		{
			this.menuTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			this.menuTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			Debug.e(e);
		}
	}

	private void loadMenuAudio(){

	}

	private void loadMenuFonts(){
		FontFactory.setAssetBasePath("font/");
		final ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		font = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "arial.ttf", 50, true, 
				Color.WHITE, 2, Color.parseColor("#c9cb1d"));
		font.load();
	}

	// ----------------------- End Menu screen -------------------------------------

	// ----------------------- Category screen -------------------------------------
	public void loadCategoryResources(){
		loadCategoryGraphics();
		loadCategoryAudio();
		loadCategoryFonts();
	}
	
	public void loadCategoryTextures(){
		categoryTextureAtlas.load();
	}
	
	public void unloadCategoryTextures(){
		categoryTextureAtlas.unload();
	}
	
	private void loadCategoryFonts() {
		
	}

	private void loadCategoryAudio() {
		// TODO Auto-generated method stub
		
	}

	private void loadCategoryGraphics() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		categoryTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
		category_background_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(categoryTextureAtlas, activity, "background.png");
		cat_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(categoryTextureAtlas, activity, "categoryButton.png");
		instruction_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(categoryTextureAtlas, activity, "InstructionBg.png");
		play_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(categoryTextureAtlas, activity, "playButton.png");
		close_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(categoryTextureAtlas, activity, "closeButton.png");
		try 
		{
			this.categoryTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			this.categoryTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			Debug.e(e);
		}
	}
	
	// ----------------------- End Category screen -------------------------------------
	
	// ----------------------- Game screen -------------------------------------
	public void loadGameResources(){
		loadGameGraphics();
		loadGameFonts();
		loadGameAudio();
	}

	public void unloadGameTextures(){

	}

	private void loadGameGraphics(){

	}

	private void loadGameFonts(){

	}

	private void loadGameAudio(){
	}

	// ----------------------- End Game screen -------------------------------------

	// ----------------------- Result screen -------------------------------------
	public void loadResultResources(){
		ITexture mainFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		resultFont = FontFactory.createStrokeFromAsset(activity.getFontManager(), mainFontTexture, activity.getAssets(), "arial.ttf", GameConstants.RESULT_TEXT_SIZE, true, 
				Color.WHITE, 2, Color.parseColor("#c9cb1d"));
		resultFont.load();
		
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/result/");
		resultTextureAtlas = new BuildableBitmapTextureAtlas(activity.getTextureManager(), 1024, 1024, TextureOptions.BILINEAR);
		result_HUD_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(resultTextureAtlas, activity, "HUDResult.png");
		next_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(resultTextureAtlas, activity, "nextButton.png");
		gameover_mode_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(resultTextureAtlas, activity, "InstructionBg.png");
		back_button_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(resultTextureAtlas, activity, "backButton.png");
		try 
		{
			this.resultTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			this.resultTextureAtlas.load();
		} 
		catch (final TextureAtlasBuilderException e)
		{
			Debug.e(e);
		}
	}
	
	public void unloadGameResources(){
		resultFont.unload();

		resultTextureAtlas.unload();
	}
	// ----------------------- End Result screen -------------------------------------

	/**
	 * @param engine
	 * @param activity
	 * @param camera
	 * @param vbom
	 * <br><br>
	 * We use this method at beginning of game loading, to prepare Resources Manager properly,
	 * setting all needed parameters, so we can latter access them from different classes (eg. scenes)
	 */
	public static void prepareManager(Engine engine, GameActivity activity, Camera camera, VertexBufferObjectManager vbom)
	{
		getInstance().engine = engine;
		getInstance().activity = activity;
		getInstance().camera = camera;
		getInstance().vbom = vbom;
	}

	//---------------------------------------------
	// GETTERS AND SETTERS
	//---------------------------------------------

	public static ResourcesManager getInstance()
	{
		return INSTANCE;
	}	
}
