package vn.tictocproduction.charades.manager;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.andengine.util.debug.Debug;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import vn.tictocproduction.charades.base.BaseScene;
import vn.tictocproduction.charades.base.Category;
import vn.tictocproduction.charades.game.GameConstants;
import vn.tictocproduction.charades.scene.CategoryScene;

public class CategoryManager {
	private static CategoryManager INSTANCE;
	private static boolean isConnected = false;

	private List<Category> categories = new ArrayList<Category>();

	//------------------------------- CATEGORY LOADER ----------------------------------------
	public List<Category> loadAllCategories(final CategoryScene scene){
		categories.clear();
		isConnected = isConnected(GameConstants.CATS + ".xml");
		if (isConnected){
			CategoryParser parser = new CategoryParser(updateData(GameConstants.CATS + ".xml"));
			Debug.e("CatManager", "words: " + parser.getCatsList().size());
			categories = parser.getCatsList();	
		}
		else {
			CategoryParser parser = new CategoryParser(readFileFromAssets("categories/categories.xml"));
			Debug.e("CatManager", "words: " + parser.getCatsList().size());
			categories = parser.getCatsList();
		}
		
		return categories;
	}
	
	public List<String> loadWordList(String catId, final BaseScene scene){
		List<String> words = new ArrayList<String>();

		if (isConnected){
			WordParser parser = new WordParser(updateData(GameConstants.WORDS + catId + ".xml"));
			words = parser.getWordsList();
		}
		else {
			WordParser parser = new WordParser(readFileFromAssets("categories/" + catId + ".xml"));
			words = parser.getWordsList();
		}

		return words;
	}

	private String readFileFromAssets(String filePath){
		String content = "";
		try {
			StringBuilder buf=new StringBuilder();
			InputStream json= ResourcesManager.getInstance().activity.getAssets().open(filePath); //"book/contents.json"
			BufferedReader in= new BufferedReader(new InputStreamReader(json));
			String str;
			while ((str=in.readLine()) != null) {
				buf.append(str);
			}
			content = buf.toString();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}

	private String updateData(String path){
		try {
			URL url = new URL(path);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());

			int bytesRead=0;
			String strFileContents = "";
			byte[] contents = new byte[1024];
			while( (bytesRead = in.read(contents)) != -1){
				strFileContents += new String(contents, 0, bytesRead);
			}
			urlConnection.disconnect();
			return strFileContents;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	private boolean isConnected(String domain){
		ConnectivityManager cm = (ConnectivityManager) 
				ResourcesManager.getInstance().activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null
		// otherwise check if we are connected
		if (networkInfo != null && networkInfo.isConnected()) {
			URL url;
			try {
				url = new URL(domain);
				HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
				urlc.setConnectTimeout(GameConstants.CONNECTION_TIME_OUT_MS);
				urlc.connect();
				if (urlc.getResponseCode() == 200) {
					Debug.e("Connection", "Success !");
					return true;
				} else {
					Debug.e("Connection", "Failed !");
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return false;
	}

	//------------------------------- END CATEGORY LOADER ----------------------------------------

	//---------------------------------------------
	// GETTERS AND SETTERS
	//---------------------------------------------

	public static CategoryManager getInstance()
	{
		if (INSTANCE == null){
			INSTANCE = new CategoryManager();
		}

		return INSTANCE;
	}
}
