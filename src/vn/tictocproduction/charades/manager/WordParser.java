package vn.tictocproduction.charades.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.andengine.util.debug.Debug;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import vn.tictocproduction.charades.game.GameConstants;

public class WordParser extends DefaultHandler{

	private List<String> words;

	public List<String> getWordsList(){ return words; }

	public WordParser(String data){
		parseDocument(data);
	}
	
	private void parseDocument(String XMLData){
		SAXParserFactory factory=SAXParserFactory.newInstance();
		SAXParser sp;
		try {
			sp = factory.newSAXParser();
			XMLReader reader=sp.getXMLReader();
			reader.setContentHandler(this);
			
			BufferedReader br=new BufferedReader(new StringReader(XMLData));
	        InputSource is=new InputSource(br);
	        is.setEncoding("UTF-8");
			reader.parse(is);
		} catch (ParserConfigurationException e) {
			Debug.e("Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (SAXException e) {
			Debug.e("Exception:" + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			Debug.e("Exception:" + e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void startDocument() throws SAXException {
		words = new ArrayList<String>();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if (qName.equals(GameConstants.TAG_WORD)){
			String wordName = attributes.getValue(GameConstants.TAG_ATTRIBUTE_NAME);
			words.add(wordName);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

	}

	// Read the value of each xml NODE
	// @param ch
	// @param start
	// @param length
	// @throws SAXException
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

	}

}
